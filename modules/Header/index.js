import Logo from "../../components/molecules/Logo/index";
import Menu from "../../components/molecules/Menu/index";
import Link from "../../components/UI/Link/index";
import Image from "../../components/UI/Image/index";
import Icon from "../../components/UI/Icon/index";
export default function index() {
  return (
    <div className="header-area">
      <div className="header-middle">
        <div className="container-xl">
          <div className="row align-items-center">
            <div className="col-7 col-sm-9 col-md-3 col-xl-2 order-1 order-md-1">
              <div className="logo">
                <Logo
                  href="/"
                  src="https://lab.lepape.com/static/frontend/Lepape/default/fr_FR/images/logo.svg"
                  alt="Lepape équipement Running, Trail, Vélo, Triathlon, Rando, Fitness"
                  imgClassName="img-responsive"
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-7 col-xl-6 order-3 order-md-2">
              <div className="header-middle-inner">
                <div className="search-container">
                  <form action="#">
                    <input
                      className="form-control form-control-lg"
                      placeholder="Que recherchez-vous ?"
                      type="text"
                    />
                  </form>
                </div>
              </div>
            </div>
            <div className="col-5 col-sm-3 col-md-2 col-xl-4 order-2 order-md-3">
              <div className="box-icone">
                <ul className="nav">
                  <li className="settings nav-item">
                    <Link href="#?">
                      <Image
                        src="https://lab.lepape.com/static/frontend/Lepape/default/fr_FR/Lepape_Base/images/flags/fr.svg"
                        alt="fr"
                        height="26"
                        width="34.2"
                      />
                      <span>France</span>
                    </Link>
                  </li>
                  <li className="settings store nav-item">
                    <Link href="#?" title="Se connecter">
                      <Icon className="ico ico-connected" />
                      <span>Se connecter</span>
                    </Link>
                  </li>
                  <li className="settings nav-item">
                    <Link href="#?" title="Se connecter">
                      <Icon className="ico ico-connected" />
                      <span>Se connecter</span>
                    </Link>
                  </li>
                  <li className="settings nav-item">
                    <div className="blockcart">
                      <Link href="/checkout/cart" title="Panier">
                        <Icon className="ico ico-basket-full ico-basket" />
                        <span>Panier</span>
                      </Link>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Menu />
    </div>
  );
}
