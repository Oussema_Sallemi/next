import { Fragment } from "react";
import Header from "../components/header/Header";
import Footer from "../components/footer/Footer";
export default function Main({ children }) {
  return (
    <Fragment>
      <Header />
      <main>{children}</main>
      <Footer />
    </Fragment>
  );
}
