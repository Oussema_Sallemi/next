import Main from "../layouts/Main";
import Head from "next/head";
import { Fragment } from "react";

// App Global Styles
import "../styles/app.scss";

function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <Head>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link rel="icon" href="favicon.ico" />
      </Head>
      <Main>
        <Component {...pageProps} />
      </Main>
    </Fragment>
  );
}

export default MyApp;
