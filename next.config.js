const path = require("path");

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
    prependData: `
    @import "_bootstrap_variable_overrides";
    @import "bootstrap/mixins/breakpoints";
`,
  },
};
