import { Link } from "../index";
const Logo = ({ logoClassName, imgClassName, alt, src, href }) => {
  return (
    <div className={logoClassName}>
      <h1>
        <Link href={href}>
          <img src={src} alt={alt} className={imgClassName} />
        </Link>
      </h1>
    </div>
  );
};

export default Logo;
