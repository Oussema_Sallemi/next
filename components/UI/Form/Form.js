export default function Form({ action, className, children, id }) {
  return (
    <form id={id} className={className} action={action}>
      {children}
    </form>
  );
}
