const Input = ({ className, id, inputName, placeholder, type }) => {
  return (
    <input
      className={className}
      type={type}
      id={id}
      name={inputName}
      placeholder={placeholder}
    />
  );
};

export default Input;
