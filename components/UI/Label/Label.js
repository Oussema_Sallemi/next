const Label = ({ className, labelFor, children }) => {
  return (
    <label className={className} htmlFor={labelFor}>
      {children}
    </label>
  );
};

export default Label;
