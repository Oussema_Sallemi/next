import NextLink from "next/link";

const Link = ({
  className,
  href,
  children,
  as,
  locale,
  title,
  onClick,
  target,
}) => {
  return (
    <NextLink href={href} as={as} locale={locale}>
      <a className={className} title={title} onClick={onClick} target={target}>
        {children}
      </a>
    </NextLink>
  );
};

export default Link;
