const Button = ({ className, onClick, type, disabled, children, title }) => {
  return (
    <button
      type={type}
      title={title}
      disabled={disabled}
      className={className}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
