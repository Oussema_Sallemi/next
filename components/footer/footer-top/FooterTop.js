import { Button, Form, Icon, Input, Label, Link } from "../../UI";

import Styles from "./FooterTop.module.scss";

export default function FooterTop() {
  return (
    <div className={`${Styles["footer-top"]}`}>
      <div className="container-xl">
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-md-3 col-12">
                <div className={Styles["widgets_container"]}>
                  <h6>DÉCOUVREZ NOS MAGASINS</h6>
                  <div className={Styles["box-footer"]}>
                    <div className={Styles["sub-title"]}>
                      <strong>LEPAPE</strong> STORE
                    </div>
                    <ul>
                      <li>
                        <span className={Styles["city"]}>
                          <strong>LEPAPE</strong> STORE PARIS
                        </span>
                        <span className={Styles["street"]}>Champs-élysées</span>
                        <Link href={"https://migration.lepape.com/store/paris"}>
                          En savoir plus
                        </Link>
                      </li>
                      <li>
                        <span className={Styles["city"]}>
                          <strong>LEPAPE</strong> STORE Lyon
                        </span>
                        <span className={Styles["street"]}>république</span>
                        <Link href={"https://migration.lepape.com/store/lyon"}>
                          En savoir plus
                        </Link>
                      </li>
                    </ul>
                  </div>
                  <div className={Styles["box-footer"]}>
                    <div className={Styles["sub-title"]}>
                      <strong>EN SELLE MARCEL</strong> PARIS
                    </div>
                    <ul>
                      <li>
                        <span className={Styles["street"]}>
                          vélos urbains, vélos pliants <br />
                          vélos tout chemin, VAE
                        </span>
                        <Link
                          className={Styles["sub-title"]}
                          href={"https://migration.lepape.com/stores"}
                        >
                          Voir les magasins
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-12">
                <div className={Styles["widgets_container"]}>
                  <h6>RESTONS EN CONTACT</h6>
                  <div className={Styles["box-footer"]}>
                    ​<div className={Styles["sub-title"]}>NEWSLETTER</div>
                    <p>
                      Abonnez-vous à notre newsletter pour RECEVOIR nos
                      sélections personnalisées, nouveautés, promotions et
                      conseils
                    </p>
                    <div className={Styles["newsletter-box"]}>
                      <Form id={"mc-form"} className={"mc-form"}>
                        <p className={Styles["adress"]}>Votre adresse email </p>
                        <Input
                          type={"email"}
                          id={"mail"}
                          className={Styles["email-box"]}
                          placeholder={"Email..."}
                          name={"mail"}
                        />
                        <div className={Styles["form-check"]}>
                          <div
                            className={Styles["custom-checkbox"]}
                            role="checkbox"
                            aria-checked="false"
                          >
                            <Input
                              className={Styles["form-check-input"]}
                              type={"checkbox"}
                              id={"news"}
                            />
                            <span className={"checkmark"}></span>
                            <Label
                              className={Styles["form-check-label"]}
                              htmlFor={"news"}
                            >
                              Je souhaite recevoir la newsletter de Lepape par
                              e-mail. Pour plus d'informations, consulter notre
                              <Link
                                href={"#?"}
                                title={"Politique de confidentialité"}
                              >
                                Politique de confidentialité
                              </Link>
                            </Label>
                          </div>
                        </div>
                        <Button
                          id={"mc-submit"}
                          className={Styles["newsletter-btn"]}
                          type={"submit"}
                        >
                          Je m'abonne
                        </Button>
                      </Form>
                    </div>
                    <div className={Styles["sub-title"]}>
                      SUR LES RÉSEAUX SOCIAUX
                    </div>
                    <ul className={`d-flex ${Styles["footer_social"]}`}>
                      <li>
                        <Link
                          href={"https://www.facebook.com/Lepape.info"}
                          target={"_blank"}
                        >
                          <Icon className={"ico ico-facebook"}></Icon>
                        </Link>
                      </li>
                      <li>
                        <Link
                          href={"https://www.instagram.com/lepape.france/"}
                          target={"_blank"}
                        >
                          <Icon className={"ico ico-instagram"}></Icon>
                        </Link>
                      </li>
                      <li>
                        <Link
                          href={"https://twitter.com/lepapeinfo"}
                          target={"_blank"}
                        >
                          <Icon className={"ico ico-twitter"}></Icon>
                        </Link>
                      </li>
                      <li>
                        <Link
                          href={"https://www.youtube.com/user/lepapeinfovideo"}
                          target={"_blank"}
                        >
                          <Icon className={"ico ico-youtube3"}></Icon>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-12">
                <div className={Styles["widgets_container"]}>
                  <h6>PROFITEZ DE NOS SERVICES</h6>
                  <div className={Styles["footer_menu"]}>
                    <ul>
                      <li>
                        <Link href={"/"}>
                          Livraison 24/48h offerte dès 90€ d'achat
                        </Link>
                      </li>
                      <li>
                        <Link href={"/"}>
                          Paiement CB sécurisé en 2,3,4 x sans frais
                        </Link>
                      </li>
                      <li>
                        <Link href={"/"}>Echanges rapides et gratuits</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Conditions générales de vente</Link>
                      </li>
                      <li>
                        <Link href={"/"}>
                          Nos codes promo et avantages fidélité
                        </Link>
                      </li>
                      <li>
                        <Link href={"/"}>
                          Déstockage - plus de 40% de remise
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-3 col-12">
                <div
                  className={`${Styles["widgets_container"]} ${Styles["space-l"]}`}
                >
                  <h6>DÉCOUVRIR</h6>
                  <div className={Styles["footer_menu"]}>
                    <ul>
                      <li>
                        <Link href={"/"}>L'entreprise</Link>
                      </li>
                      <li>
                        <Link href={"/"}>LEPAPE PRO</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Nos offres d'emploi</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Partenaires</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Programme d'affiliation</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Conseils triathlon</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Conseils home trainers</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Idées cadeaux Vélo</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Black Friday</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Soldes Running & Trail</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Soldes Vélo</Link>
                      </li>
                      <li>
                        <Link href={"/"}>Jeux concours</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
