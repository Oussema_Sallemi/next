import Styles from "./FooterBottom.module.scss";

export default function FooterBottom() {
  return (
    <div className={Styles["footer-bottom"]}>
      <div className="container-xl">
        <div className="row align-items-center">
          <div className=" col-12">
            <div className={Styles["footer-bottom-content"]}>
              <div
                className={`${Styles["footer-copyright"]} ${Styles["text-center"]}`}
              >
                <p>
                  © Copyright LEPAPE 1996-2020 | 39, rue d´Artois - 75008 Paris
                  <br />
                  Tel. : 01 53 75 00 00 | info@lepape.com <br />
                  SAS Au capital de 104 246.20 € <br />
                  R.C.S. Paris : 439 656 976
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
