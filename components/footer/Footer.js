import FooterTop from "./footer-top/FooterTop";
import FooterBottom from "./footer-bottom/FooterBottom";
const Footer = () => {
  return (
    <footer>
      <FooterTop />
      <FooterBottom />
    </footer>
  );
};

export default Footer;
