import { Icon, Link } from "../../UI";
import Styles from "./Menu.module.scss";

export default function Menu() {
  return (
    <div className={Styles["header-bottom-area"]}>
      <div className="container-xl">
        <div className="row">
          <div className="col-lg-12">
            <Link href="#menu" className={Styles["menu-icon"]}>
              <Icon className={"ico ico-burger-button"}></Icon>
            </Link>
            <nav className={Styles["main-menu"]} id="menu">
              <ul>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    running & traiL
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    cyclisme
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    randonnée
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    fitness
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    natation
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    Montres cardio & GPS
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    nutrition & soin
                  </Link>
                </li>
                <li className={`${Styles["level1"]} ${Styles["bg-promo"]}`}>
                  <Link href={"/"} className={Styles["level1"]}>
                    promos
                  </Link>
                </li>
                <li className={Styles["level1"]}>
                  <Link href={"/"} className={Styles["level1"]}>
                    marques
                  </Link>
                </li>
                <li className={`${Styles["level1"]} ${Styles["link-store"]}`}>
                  <Link href="/" className={Styles["level1"]}>
                    <Icon class={"ico ico-store"}></Icon>
                    <span>Magasins</span>
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
}
