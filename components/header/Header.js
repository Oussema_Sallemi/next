import Menu from "./menu/Menu";
import { Form, Icon, Input, Link, Logo } from "../UI";

import Styles from "./Header.module.scss";

const Header = ({}) => {
  return (
    <header>
      <div className={Styles["header-area"]}>
        <div className={Styles["header-middle"]}>
          <div className="container-xl">
            <div className="row align-items-center">
              <div className="col-7 col-sm-9 col-md-3 col-xl-2 order-1 order-md-1">
                <Logo
                  logoClassName={Styles["logo"]}
                  imgClassName={"img-fluid"}
                  alt={
                    "Lepape équipement Running, Trail, Vélo, Triathlon, Rando, Fitness"
                  }
                  src={"/img/logo.svg"}
                  href={"/"}
                />
              </div>
              <div className="col-12 col-sm-12 col-md-7 col-xl-6 order-3 order-md-2">
                <div className={Styles["header-middle-inner"]}>
                  <Form action={"#"}>
                    <Input
                      className={"form-control form-control-lg"}
                      placeholder={"Que recherchez-vous ?"}
                      type={"text"}
                    />
                  </Form>
                </div>
              </div>
              <div className="col-5 col-sm-3 col-md-2 col-xl-4 order-2 order-md-3">
                <div className={Styles["box-icone"]}>
                  <ul className="nav">
                    <li className={`${Styles["settings"]} nav-item`}>
                      <Link href="#?">
                        <img src="/img/fr.svg" />
                        <span>France</span>
                      </Link>
                    </li>
                    <li className={`${Styles["settings"]} store nav-item`}>
                      <Link href="#?">
                        <Icon className={"ico ico-store"} />
                        <span>Magasins</span>
                      </Link>
                    </li>
                    <li className={`${Styles["settings"]} nav-item`}>
                      <Link href="#?" title="Se connecter">
                        <Icon className={"ico ico-connected"} />
                        <span>Connexion</span>
                      </Link>
                    </li>
                    <li className={`${Styles["settings"]} nav-item`}>
                      <div className="blockcart">
                        <Link href="#?" className="drop-toggle">
                          <Icon className={"ico ico-basket-full ico-basket"} />
                          <span>Connexion</span>
                        </Link>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Menu />
      </div>
    </header>
  );
};

export default Header;
